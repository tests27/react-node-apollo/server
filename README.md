# Prerequisites:

- Postgres on port 5432
- Postgres database "samruk"
  -- you can also edit postgres connection config at /prisma/.env
- Redis server on 6379

# Run application:

> yarn

> yarn migrate

> yarn generate

> yarn dev2
