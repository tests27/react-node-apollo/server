import { AuthenticationError } from "apollo-server-express";
import { MiddlewareFn } from "type-graphql";
import MyContext from "../../types/MyContext";

export const IsAuthorized: MiddlewareFn<MyContext> = ({ context }, next) => {
  const user = context.user;
  if (user) {
    return next();
  }
  throw new AuthenticationError("Access denied");
};
