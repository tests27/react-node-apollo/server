import { Request } from "express";
import { prisma } from "../../prismaClient";
import { User } from "../../generated";

// Get user instance from Express request object
export async function getUserFromReq(req: Request): Promise<User | null> {
  const userId = req.session?.userId;
  if (userId) {
    const user = await prisma.user.findUnique({ where: { id: userId } });
    if (user) {
      return user;
    }
  }
  return null;
}
