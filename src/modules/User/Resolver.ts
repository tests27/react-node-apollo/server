import {
  Args,
  ArgsType,
  Ctx,
  Field,
  InputType,
  Mutation,
  ObjectType,
  Resolver,
  UseMiddleware
} from "type-graphql";
import argon2 from "argon2";
import MyContext from "../../types/MyContext";
import { __COOKE_NAME__ } from "../../constants";
import { User } from "../../generated";
import { IsAuthorized } from "./middleware";

@InputType()
@ArgsType()
class UserCredentialsInput {
  @Field(() => String)
  username!: string;

  @Field(() => String)
  password!: string;
}

@ObjectType()
class FieldError {
  @Field()
  field: string;

  @Field()
  message: string;
}

@ObjectType()
class UserResponse {
  @Field(() => [FieldError], { nullable: true })
  errors?: FieldError[];

  @Field(() => User, { nullable: true })
  user?: User;
}

@Resolver(User)
export class UserResolver {
  @Mutation(() => UserResponse)
  async register(
    @Ctx() { prisma }: MyContext,
    @Args(() => UserCredentialsInput)
    { username, password }: UserCredentialsInput
  ): Promise<UserResponse> {
    const hashedPassword = await argon2.hash(password);
    let user;
    try {
      // TODO: Validate if user exists
      const existingUser = await prisma.user.findUnique({
        where: {
          username: username
        }
      });
      if (!!existingUser) {
        return {
          errors: [
            { field: "username", message: "Имя пользователя уже занято" }
          ]
        };
      }

      user = await prisma.user.create({
        data: {
          username: username,
          password: hashedPassword
        }
      });
    } catch (err) {
      console.log(err);
    }

    return {
      user
    };
  }

  @Mutation(() => UserResponse)
  async login(
    @Ctx() { prisma, req }: MyContext,
    @Args(() => UserCredentialsInput)
    { username, password }: UserCredentialsInput
  ): Promise<UserResponse> {
    const user = await prisma.user.findUnique({
      where: {
        username: username
      }
    });

    if (!user) {
      return {
        errors: [
          { field: "username", message: "Такого тользователя не существует" }
        ]
      };
    }

    const valid = await argon2.verify(user.password, password);
    if (!valid) {
      return {
        errors: [
          {
            field: "password",
            message: "Неверный пароль"
          }
        ]
      };
    }

    req.session.userId = user.id;

    return { user };
  }

  @UseMiddleware(IsAuthorized)
  @Mutation(() => Boolean)
  logout(@Ctx() { req, res }: MyContext): Promise<boolean> {
    return new Promise((resolve) =>
      req.session.destroy((err) => {
        res.clearCookie(__COOKE_NAME__);
        if (err) {
          console.log(err);
          resolve(false);
          return;
        }

        resolve(true);
      })
    );
  }
}
