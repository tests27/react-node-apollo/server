import {
  Args,
  ArgsType,
  Field,
  InputType,
  Query,
  Resolver,
  UseMiddleware
} from "type-graphql";
import { __COOKE_NAME__ } from "../../constants";
import { User } from "../../generated";
import { IsAuthorized } from "../User/middleware";

@InputType()
@ArgsType()
class TestInput {
  @Field(() => [Number])
  arr!: number[];

  @Field(() => Number)
  n!: number;
}

@Resolver(User)
export class ProblemResolver {
  @UseMiddleware(IsAuthorized)
  @Query(() => [Number])
  solve(@Args(() => TestInput) { arr, n }: TestInput): number[] {
    const sums = [];
    const ans: number[] = [];

    for (let i = 0; i <= arr.length - n; i++) {
      sums[i] = { [i]: arr.slice(i, i + n).reduce((acc, cur) => acc + cur, 0) };
    }

    sums.sort((sum1, sum2) =>
      Object.values(sum1)[0] > Object.values(sum2)[0] ? 1 : -1
    );

    for (let i = sums.length - 1; i >= 0; i--) {
      const idx = +Object.keys(sums[i])[0];
      if (!ans.some((a) => Math.abs(a - idx) < n)) {
        ans.push(idx);
      }
      if (ans.length >= 3) {
        break;
      }
    }

    ans.sort((a, b) => (a > b ? 1 : -1));

    return ans;
  }
}
