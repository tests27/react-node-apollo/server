import path from "path";

export const __PROD__ = process.env.NODE_ENV || "development";

export const __PORT__ = process.env.PORT || 56789;

export const __PRISMA__ = path.resolve(__dirname + "../prisma");

export const __BASEDIR__ = path.resolve(__dirname + "/../");

export const __HTTPS__ = false;

export const __COOKE_NAME__ = "qid";

export const __CLIENT_URL__ = "http://localhost:8083";
