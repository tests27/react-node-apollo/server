import { Request, Response } from "express";
import { PrismaClient } from "@prisma/client";
import { User } from "../generated";

export default interface MyContext {
  req: Request & { session: Express.Session };
  res: Response;
  prisma: PrismaClient;
  user: User | null;
}
