import "reflect-metadata";
import express from "express";
import cors from "cors";
import path from "path";
import { ApolloServer } from "apollo-server-express";
import { buildSchema } from "type-graphql";
import redis from "redis";
import session from "express-session";
import connectRedis from "connect-redis";
import history from "connect-history-api-fallback";

import { prisma } from "./prismaClient";
import {
  __CLIENT_URL__,
  __COOKE_NAME__,
  __HTTPS__,
  __PORT__
} from "./constants";

import { UserResolver } from "./modules/User/Resolver";

import { getUserFromReq } from "./modules/User/helpers";
import { ProblemResolver } from "./modules/Problem/Resolver";

(async () => {
  const app = express();

  const RedisStore = connectRedis(session);
  const redisClient = redis.createClient();

  app.use(
    cors({
      origin: [__CLIENT_URL__],
      credentials: true
    })
  );

  app.use(
    session({
      name: __COOKE_NAME__,
      store: new RedisStore({
        client: redisClient,
        disableTouch: false // When touch is enabled, everytime client makes request, his cookie resets
      }),
      cookie: {
        maxAge: 1000 * 60 * 30, // 30 minutes
        sameSite: "lax",
        httpOnly: true,
        secure: __HTTPS__ // is https?
      },
      saveUninitialized: false, // Do not create session if nothing is stored
      secret: "very-secret",
      resave: false
    })
  );

  // Static
  app.use("/static", express.static(path.join(__dirname, "..", "static")));

  const apolloServer = new ApolloServer({
    schema: await buildSchema({
      resolvers: [UserResolver, ProblemResolver],
      validate: false
    }),
    context: async ({ req, res }) => {
      let user;
      try {
        user = await getUserFromReq(req);
      } catch (err) {
        console.log(err);
      }
      return { req, res, prisma, user };
    }
  });

  apolloServer.applyMiddleware({
    app,
    cors: false
  });

  // Frontend
  app.use(history());
  app.use("/", express.static(path.join(__dirname, "..", "frontend")));

  app.listen(__PORT__, () =>
    console.log(`Server ready at http://localhost:${__PORT__}`)
  );
})().catch((err) => console.log(err));
